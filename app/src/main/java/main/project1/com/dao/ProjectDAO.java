package main.project1.com.dao;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;
import java.util.HashMap;

import main.project1.com.project1.Login;
import main.project1.com.util.HTTPURLConnection;
import main.project1.com.util.State;

/**
 * Created by CPU140252 on 20-Jan-17.
 */
public class ProjectDAO extends AsyncTask<Void, Void, Void> {
    String response = "";
    //Create hashmap Object to send parameters to web service
    HashMap<String, String> postDataParams;
    private ProgressDialog pDialog;
    private Context context;
    String username, password, idfirebase;
    private HTTPURLConnection service;
    private String path = "";
    private JSONObject json;
    private int success = 0;
    private boolean status = false;
    private int action = 0;

    public ProjectDAO(Context context, Object obj) {
        main.project1.com.model.Login lgn = (main.project1.com.model.Login) obj;
        Log.e("PRO", lgn.getUsername());
        username = lgn.getUsername();
        password = lgn.getPassword();
        idfirebase = lgn.getIdfirebase();
        Log.e("PRO", lgn.getPassword());
        Log.e("PRO", lgn.getIdfirebase());
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        service = new HTTPURLConnection();

        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Please wait...");
        pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        //pDialog.setCancelable(false);
        pDialog.show();
    }

    @Override
    protected Void doInBackground(Void... arg0) {
        insert();
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (pDialog.isShowing())
            pDialog.dismiss();
        if (success == 1 || status == true) {
            Toast.makeText(context, "Success", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(context, "Failed", Toast.LENGTH_LONG).show();
        }
    }

    private boolean login() {
        path = "http://yosisetiawan.esy.es/insert%20database/loginvalid.php";
        postDataParams = new HashMap<String, String>();
        postDataParams.put("username", username);
        postDataParams.put("password", password);
        //postDataParams.put("address", strAddress);
        //Call ServerData() method to call webservice and store result in response
        response = service.ServerData(path, postDataParams);
        Log.e("response", response);
        try {
            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1); //convert
            json = new JSONObject(response);
            status = json.getBoolean("success");
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
        return status;
    }

    private void insert() {
        path = "http://yosisetiawan.esy.es/insert%20database/addemployee.php";
        postDataParams = new HashMap<String, String>();
        postDataParams.put("username", username);
        postDataParams.put("password", password);
        postDataParams.put("idfirebase", idfirebase);
        //postDataParams.put("address", strAddress);
        //Call ServerData() method to call webservice and store result in response
        response = service.ServerData(path, postDataParams);
        try {
            response = response.substring(response.indexOf("{"), response.lastIndexOf("}") + 1); //convert
            json = new JSONObject(response);
            success = json.getInt("success");

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("error", "error");
        }
    }
}