package main.project1.com.model;

/**
 * Created by Yossi on 26-Jan-17.
 */
public class User {
    private String nama;
    private String area;
    private String kodePegawai;
    private String kodeArea;
    private String TTL;
    private String alamat;
    private String phone;
    private String tglDaftar;

    public User(String nama, String area, String kodePegawai,
                String kodeArea, String TTL, String alamat, String phone, String tglDaftar) {
        this.nama = nama;
        this.area = area;
        this.kodePegawai = kodePegawai;
        this.kodeArea = kodeArea;
        this.TTL = TTL;
        this.alamat = alamat;
        this.phone = phone;
        this.tglDaftar = tglDaftar;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getKodePegawai() {
        return kodePegawai;
    }

    public void setKodePegawai(String kodePegawai) {
        this.kodePegawai = kodePegawai;
    }

    public String getKodeArea() {
        return kodeArea;
    }

    public void setKodeArea(String kodeArea) {
        this.kodeArea = kodeArea;
    }

    public String getTTL() {
        return TTL;
    }

    public void setTTL(String TTL) {
        this.TTL = TTL;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTglDaftar() {
        return tglDaftar;
    }

    public void setTglDaftar(String tglDaftar) {
        this.tglDaftar = tglDaftar;
    }
}
