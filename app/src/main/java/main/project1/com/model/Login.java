package main.project1.com.model;

/**
 * Created by CPU140252 on 25-Jan-17.
 */
public class Login {
    private String username;
    private String password;
    private String idfirebase;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdfirebase() {
        return idfirebase;
    }

    public void setIdfirebase(String idfirebase) {
        this.idfirebase = idfirebase;
    }
}
