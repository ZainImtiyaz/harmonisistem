package main.project1.com.model;

/**
 * Created by Yossi on 26-Jan-17.
 */
public class Customer {
    private String nama;
    private String area;
    private String kodePelanggan;
    private String kodeArea;
    private String jenisLayanan;
    private int jumlahUnit;
    private String alamat;
    private String phone;
    private String tglDaftar;
    private String PIC;

    public Customer(String nama, String area, String kodePelanggan,
                    String kodeArea, String jenisLayanan, int jumlahUnit,
                    String alamat, String phone, String tglDaftar, String PIC) {
        this.nama = nama;
        this.area = area;
        this.kodePelanggan = kodePelanggan;
        this.kodeArea = kodeArea;
        this.jenisLayanan = jenisLayanan;
        this.jumlahUnit = jumlahUnit;
        this.alamat = alamat;
        this.phone = phone;
        this.tglDaftar = tglDaftar;
        this.PIC = PIC;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getKodePelanggan() {
        return kodePelanggan;
    }

    public void setKodePelanggan(String kodePelanggan) {
        this.kodePelanggan = kodePelanggan;
    }

    public String getKodeArea() {
        return kodeArea;
    }

    public void setKodeArea(String kodeArea) {
        this.kodeArea = kodeArea;
    }

    public String getJenisLayanan() {
        return jenisLayanan;
    }

    public void setJenisLayanan(String jenisLayanan) {
        this.jenisLayanan = jenisLayanan;
    }

    public int getJumlahUnit() {
        return jumlahUnit;
    }

    public void setJumlahUnit(int jumlahUnit) {
        this.jumlahUnit = jumlahUnit;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTglDaftar() {
        return tglDaftar;
    }

    public void setTglDaftar(String tglDaftar) {
        this.tglDaftar = tglDaftar;
    }

    public String getPIC() {
        return PIC;
    }

    public void setPIC(String PIC) {
        this.PIC = PIC;
    }
}
