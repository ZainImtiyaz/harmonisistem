package main.project1.com.project1;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import main.project1.com.util.State;

/**
 * Created by CPU140252 on 09-Jan-17.
 */
public class Report extends Activity {
    private TextView txtusername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainreport);
        txtusername = (TextView) findViewById(R.id.txtusername);
        txtusername.setText(getString(R.string.welcome) + State.username);
    }
}
