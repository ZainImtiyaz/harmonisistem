package main.project1.com.project1;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by CPU140252 on 09-Jan-17.
 */
public class Home extends Fragment implements View.OnClickListener {
    private Button btnJob, btnPerformance, btnReport;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //returning our layout file
        //change R.layout.yourlayoutfilename for each of your fragments
        View v = inflater.inflate(R.layout.home, container, false);
        btnJob = (Button) v.findViewById(R.id.btnJob);
        btnPerformance = (Button) v.findViewById(R.id.btnPerformance);
        btnReport = (Button) v.findViewById(R.id.btnReport);
        btnJob.setOnClickListener(this);
        btnPerformance.setOnClickListener(this);
        btnReport.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.btnJob:
//                Toast.makeText(getActivity(), "Job", Toast.LENGTH_SHORT).show();
                intent = new Intent(getActivity(), MainJob.class);
                startActivity(intent);
                break;
            case R.id.btnPerformance:
                intent = new Intent(getActivity(), Performance.class);
                startActivity(intent);
//                Toast.makeText(getActivity(), "Performance", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btnReport:
//                Toast.makeText(getActivity(), "Report", Toast.LENGTH_SHORT).show();
                intent = new Intent(getActivity(), Report.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //you can set the title for your toolbar here for different fragments different titles
        getActivity().setTitle("Home");
    }
}
