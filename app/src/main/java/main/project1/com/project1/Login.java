package main.project1.com.project1;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import main.project1.com.dao.ProjectDAO;
import main.project1.com.util.Config;
import main.project1.com.util.Connection;
import main.project1.com.util.GPSTracker;
import main.project1.com.util.HTTPURLConnection;
import main.project1.com.util.NotificationUtils;
import main.project1.com.util.State;

/**
 * Created by CPU140252 on 09-Jan-17.
 */
public class Login extends Activity {
    private TextView txtusername, txtpassword;
    private Button btnlogin;
    private static String ADMIN = "admin";
    private static final String TAG = Login.class.getSimpleName();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    GPSTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        //code here
        txtusername = (TextView) findViewById(R.id.txtusername);
        txtpassword = (TextView) findViewById(R.id.txtpassword);
        btnlogin = (Button) findViewById(R.id.btnllogin);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //register
//                Object object = null;
//                main.project1.com.model.Login lgn = new main.project1.com.model.Login();
//                lgn.setUsername(txtusername.getText().toString());
//                lgn.setPassword(txtpassword.getText().toString());
//                lgn.setIdfirebase(displayFirebaseRegId());
//                if (displayFirebaseRegId() != "") {
//                    object = lgn;
//                    ProjectDAO dao = new ProjectDAO(Login.this, object);
//                    dao.execute();
//                } else {
//                    Toast.makeText(Login.this, "Wait Id Firebase", Toast.LENGTH_LONG).show();
//                }
                //register

                //gps
//                gps = new GPSTracker(Login.this);
//
//                // check if GPS enabled
//                if (gps.canGetLocation()) {
//
//                    double latitude = gps.getLatitude();
//                    double longitude = gps.getLongitude();
//
//                    // \n is for new line
//                    Toast.makeText(getApplicationContext(), "Your Location is - \nLat: " + latitude + "\nLong: " + longitude, Toast.LENGTH_LONG).show();
//                } else {
//                    // can't get location
//                    // GPS or Network is not enabled
//                    // Ask user to enable GPS/network in settings
//                    gps.showSettingsAlert();
//                }
                //end gps

                //login
//                Object object = null;
//                main.project1.com.model.Login lgn = new main.project1.com.model.Login();
//                lgn.setUsername("Yossi");
//                lgn.setPassword("1234");
//                object = lgn;
//                ProjectDAO dao = new ProjectDAO(Login.this, object);
//                dao.execute();
                //end login

                String username = txtusername.getText().toString();
                String password = txtpassword.getText().toString();
                Connection connection = new Connection(Login.this);
                if (connection.isConnectToInternet()) {
                    if (connection.isActivateInternetConnection()) {
                        if (username.equalsIgnoreCase(ADMIN) && password.equalsIgnoreCase(ADMIN)) {
                            Intent intent = new Intent(Login.this,
                                    MainActivity.class);
                            intent.putExtra("username", username);
                            startActivity(intent);
                            finish();
                        } else {
                            Toast.makeText(Login.this, getString(R.string.notmatch), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(Login.this, "Please Check Your Internet Connection", Toast.LENGTH_LONG);
                    }
                } else {
                    Toast.makeText(Login.this, "Please Go to Setting and Active the Mobile Data or WiFi", Toast.LENGTH_LONG).show();
                }
            }
        });

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        displayFirebaseRegId();
    }

    // Fetches reg id from shared preferences
    // and displays on the screen
    private String displayFirebaseRegId() {
        String idfirebase = "";
        //Firebast id selalu berubah ubah jika Share_Pref
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);

        if (!TextUtils.isEmpty(regId)) {
            //txtRegId.setText("Firebase Reg Id: " + regId);
            Toast.makeText(this, "Firebase Reg Id: " + regId, Toast.LENGTH_LONG).show();
            idfirebase = regId;
        } else {
            Toast.makeText(this, "Firebase Reg Id is not received yet.", Toast.LENGTH_LONG).show();
        }
        return idfirebase;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
